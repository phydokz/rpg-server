var defaultModel = require('rpg-node-mvc').Model;
var deepmerge = require('deepmerge');
var Schema = require('mongoose').Schema;
var crypto = require('crypto');


var Animation = {
    name : 'Animation',
    _messages : {
        file:{
            required:'O arquivo deve ser enviado!',
            unique:'O nome do arquivo deve ser único!'
        },
        name:{
            required:'O nome deve ser informado!',
            unique:'Já existe uma Animação com esse nome!'
        }
    },
    _schema : {
       name:{
            type:String,
            required:true,
            unique:true
       },
       file:{
            type:String,
            required:true,
            unique:true
       },
       created:{
           type:Date,
           required:true,
           default:Date.now
       }
    },
    _statics: {
        generateUniqueName: function (callback) {
            var self = this;
            var name = crypto.randomBytes(20).toString('hex')+'.anim';

            self.find({name:name},function (err, docs) {
                if(err){
                    callback(err,name);
                }
                else if (!docs.length) {
                    callback(err,name);
                }
                else {
                    self.generateUniqueName(callback);
                }
            });
        }
    },
    defaultConnection:'rpgbuilder'
};

Animation = deepmerge(defaultModel,Animation);
module.exports = Animation;