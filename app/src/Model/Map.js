var defaultModel = require('rpg-node-mvc').Model;
var deepmerge = require('deepmerge');
var crypto = require('crypto');

var Map = {
    name : 'Map',
    _messages : {
        file:{
            required:'O arquivo deve ser enviado!'
        },
        name:{
            required:'O nome deve ser informado!',
            unique:'Já existe um mapa com esse nome!'
        }
    },
    _schema : {
        file:{
            type:String,
            required:true,
            trim:true,
            unique:true
        },
        name:{
            type:String,
            required:true,
            trim:true,
            unique:true
        },
        created:{
            type:Date,
            required:true,
            default:Date.now
        }
    },
    _statics: {
        generateUniqueName: function (callback) {
            var self = this;
            var name = crypto.randomBytes(20).toString('hex')+'.map';

            self.find({name:name},function (err, docs) {
                if(err){
                    callback(err,name);
                }
                else if (!docs.length) {
                    callback(err,name);
                }
                else {
                    self.generateUniqueName(callback);
                }
            });
        }
    },
    defaultConnection:'rpgbuilder'
};

Map = deepmerge(defaultModel,Map);
module.exports = Map;