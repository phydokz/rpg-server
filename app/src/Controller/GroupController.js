var paths = require('rpg-node-mvc').paths;
var path = require('path');
var fs = require('fs');

module.exports = {
    __constructor:function(){
        var self = this;
        self.name = 'groups';
        self.modelName = 'Group';
    },
    methods: {
        /**
         * @Method("add");
         * @RequestMethod("POST");
         * @Uri("/");
         */
        add:function (req, res, next) {
            var self = this;
            var group = req.body.group;
            var group_name = req.body.name;

            self.Group.generateUniqueName(function(err,name){
                if(err){
                    res.end(JSON.stringify(err));
                }
                else{
                    var file = path.join(paths('webroot'),'groups',name);
                    fs.writeFile(file, group, function (err) {
                        if(err){
                            res.end(JSON.stringify(err));
                        }
                        else{
                            self.Group.save({
                                file:name,
                                name:group_name
                            }, function (err,doc) {
                                if (err) {
                                    res.end(JSON.stringify(err));
                                }
                                else {
                                    res.end(JSON.stringify({success: true,group:doc}));
                                }
                            });
                        }
                    });
                }
            });

        },
        /**
         * @Method("update");
         * @RequestMethod("POST");
         * @Uri("/update");
         * @allow("admin");
         */
        update:function(req,res,next){
            var self = this;
            var group = req.body.group;
            var id = req.body.id;

            self.Group.findOne({_id:id},function(err,doc){
                if(err || !doc){
                    res.end(JSON.stringify(err));
                }
                else{
                    var complete = path.join(paths('webroot'),'groups',doc.file);
                    fs.writeFile(complete, group, function (err) {
                        if(err){
                            res.end(JSON.stringify(err));
                        }
                        else{
                            res.end(JSON.stringify({success: true}));
                        }
                    });
                }
            });
        },
        /**
         * @Method("delete");
         * @RequestMethod("GET");
         * @RequestMethod("DELETE");
         * @Uri("/delete");
         * @allow("admin");
         */
        delete: function (req, res, next) {
            var self = this;
            var id = req.query.id;
            self.Group.remove({_id:id}, function (err) {
                if (err) {
                    res.end(JSON.stringify(err));
                }
                else {
                    res.end(JSON.stringify({success: true}));
                }
            });
        },
        /**
         * @Method("list");
         * @RequestMethod("GET");
         * @allow("admin");
         */
        list: function (req, res, next) {
            var self = this;

            self.Group.count({},function(err,c){
                if(err){
                    self.endJson({
                        success:false,
                        errors:err
                    });
                }
                else{
                    var conditions = {
                        sort:{created:'desc'}
                    };

                    var limit = req.query.limit | null;
                    var page = req.query.page | null;


                    conditions.limit = limit?limit:c;

                    if(page){
                        conditions.page = page;
                    }

                    self.Group.paginate({}, conditions).then(function(result){
                        var docs = result.docs;
                        var fullUrl = req.protocol + '://' + req.get('host')+'/'+'groups';

                        docs.forEach(function(doc,index){
                            var url = fullUrl+'/'+doc.file;
                            doc = doc.toObject();
                            doc.url = url;
                            docs[index] = doc;
                        });

                        self.endJson({
                            success:true,
                            count:c,
                            groups:docs
                        });
                    });
                }
            });
        },
        /**
         * @Method("load");
         * @RequestMethod("GET");
         * @allow("admin");
         * @Uri("/load");
         */
        load:function(req,res,next){
            var name = req.query.name;
            var self = this;
            self.Group.findOne({name:name},function(err,doc){
                if(err || !doc){
                    self.endJson({
                        success:false,
                        errors:err
                    });
                }
                else{
                    self.endJson({
                        success:true,
                        group:doc
                    });
                }
            });
        }
    }
};

