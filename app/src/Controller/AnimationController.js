/**
 * Created by pablo on 12/01/16.
 */
var paths = require('rpg-node-mvc').paths;
var path = require('path');
var fs = require('fs');

module.exports = {
    __constructor:function(){
        var self = this;
        self.name = 'animations';
        self.modelName = 'Animation'
    },
    methods:{
        /**
         * @Method("add");
         * @RequestMethod("POST");
         * @Uri("/");
         */
        add:function (req, res, next) {
            var self = this;
            var animation = req.body.animation;
            var animation_name = req.body.name;

            self.Animation.generateUniqueName(function(err,name){
                if(err){
                    res.end(JSON.stringify(err));
                }
                else{
                    var file = path.join(paths('webroot'),'animations',name);
                    fs.writeFile(file, animation, function (err) {
                        if(err){
                            res.end(JSON.stringify(err));
                        }
                        else{
                            self.Animation.save({
                                file:name,
                                name:animation_name
                            }, function (err,doc) {
                                if (err) {
                                    res.end(JSON.stringify(err));
                                }
                                else {
                                    res.end(JSON.stringify({success: true,animation:doc}));
                                }
                            });
                        }
                    });
                }
            });

        },
        /**
         * @Method("update");
         * @RequestMethod("POST");
         * @Uri("/update");
         */
        update:function(req,res,next){
            var self = this;
            var animation = req.body.animation;
            var id = req.body.id;

            self.Animation.findOne({_id:id},function(err,doc){
                if(err || !doc){
                    res.end(JSON.stringify(err));
                }
                else{
                    var complete = path.join(paths('webroot'),'animations',doc.file);
                    fs.writeFile(complete, animation, function (err) {
                        if(err){
                            res.end(JSON.stringify(err));
                        }
                        else{
                            res.end(JSON.stringify({success: true}));
                        }
                    });
                }
            });
        },
        /**
         * @Method("list");
         * @RequestMethod("GET");
         */
        list:function(req,res,next){
            var self = this;
            var conditions = {};
            self.Animation.count(conditions,function(err,c){
                if(err){
                    self.endJson({
                        success:false,
                        errors:err
                    });
                }
                else{
                    var conditions = {
                        sort:{created:'desc'}
                    };

                    var limit = req.query.limit | null;
                    var page = req.query.page | null;


                    conditions.limit = limit?limit:c;

                    if(page){
                        conditions.page = page;
                    }

                    self.Animation.paginate({}, conditions).then(function(result){
                        self.endJson({
                            success:true,
                            count:c,
                            animations:result.docs
                        });
                    });
                }
            });
        },
        /**
         * @Method("delete");
         * @RequestMethod("DELETE");
         */
        delete:function(req,res,next){
            var id = req.query.id;
            var self = this;
            self.Animation.remove({_id:id},function(err){
                self.endJson({success:!err});
            });
        },
        /**
         * @Method("load");
         * @RequestMethod("GET");
         * @allow("admin");
         * @Uri("/load");
         */
        load:function(req,res,next){
            var name = req.query.name;
            var self = this;
            self.Animation.findOne({name:name},function(err,doc){
                if(err || !doc){
                    self.endJson({
                        success:false,
                        errors:err
                    });
                }
                else{
                    self.endJson({
                        success:true,
                        animation:doc
                    });
                }
            });
        }
    }
};
