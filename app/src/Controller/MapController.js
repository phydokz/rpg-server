var paths = require('rpg-node-mvc').paths;
var path = require('path');
var fs = require('fs');

module.exports = {
    __constructor:function(){
        var self = this;
        self.name = 'maps';
        self.modelName = 'Map';
    },
    methods: {
        /**
         * @Method("add");
         * @RequestMethod("POST");
         * @Uri("/");
         */
        add:function (req, res, next) {
            var self = this;
            var map = req.body.map;
            var map_name = req.body.name;

            self.Map.generateUniqueName(function(err,name){
                if(err){
                    res.end(JSON.stringify(err));
                }
                else{
                    var file = path.join(paths('webroot'),'maps',name);
                    fs.writeFile(file, map, function (err) {
                        if(err){
                            res.end(JSON.stringify(err));
                        }
                        else{
                            self.Map.save({
                                file:name,
                                name:map_name
                            }, function (err,doc) {
                                if (err) {
                                    res.end(JSON.stringify(err));
                                }
                                else {
                                    res.end(JSON.stringify({success: true,map:doc}));
                                }
                            });
                        }
                    });
                }
            });

        },
        /**
         * @Method("update");
         * @RequestMethod("POST");
         * @Uri("/update");
         * @allow("admin");
         */
        update:function(req,res,next){
            var self = this;
            var map = req.body.map;
            var id = req.body.id;

            self.Map.findOne({_id:id},function(err,doc){
                if(err || !doc){
                    res.end(JSON.stringify(err));
                }
                else{
                    var complete = path.join(paths('webroot'),'maps',doc.file);
                    fs.writeFile(complete, map, function (err) {
                        if(err){
                            res.end(JSON.stringify(err));
                        }
                        else{
                            res.end(JSON.stringify({success: true}));
                        }
                    });
                }
            });
        },
        /**
         * @Method("delete");
         * @RequestMethod("GET");
         * @RequestMethod("DELETE");
         * @Uri("/delete");
         * @allow("admin");
         */
        delete: function (req, res, next) {
            var self = this;
            var id = req.query.id;
            self.Map.remove({_id:id}, function (err) {
                if (err) {
                    res.end(JSON.stringify(err));
                }
                else {
                    res.end(JSON.stringify({success: true}));
                }
            });
        },
        /**
         * @Method("list");
         * @RequestMethod("GET");
         * @allow("admin");
         */
        list: function (req, res, next) {
            var self = this;

            self.Map.count({},function(err,c){
                if(err){
                    self.endJson({
                        success:false,
                        errors:err
                    });
                }
                else{
                    var conditions = {
                        sort:{created:'desc'}
                    };

                    var limit = req.query.limit | null;
                    var page = req.query.page | null;


                    conditions.limit = limit?limit:c;

                    if(page){
                        conditions.page = page;
                    }

                    self.Map.paginate({}, conditions).then(function(result){
                        var docs = result.docs;
                        var fullUrl = req.protocol + '://' + req.get('host')+'/'+'maps';

                        docs.forEach(function(doc,index){
                            var url = fullUrl+'/'+doc.file;
                            doc = doc.toObject();
                            doc.url = url;
                            docs[index] = doc;
                        });

                        self.endJson({
                            success:true,
                            count:c,
                            maps:docs
                        });
                    });
                }
            });
        },
        /**
         * @Method("load");
         * @RequestMethod("GET");
         * @allow("admin");
         * @Uri("/load");
         */
        load:function(req,res,next){
            var name = req.query.name;
            var self = this;
            self.Map.findOne({name:name},function(err,doc){
                if(err || !doc){
                    self.endJson({
                        success:false,
                        errors:err
                    });
                }
                else{
                    self.endJson({
                        success:true,
                        map:doc
                    });
                }
            });
        }
    }
};

